package com.handson.bidkraft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.entities.Review;
import com.handson.bidkraft.responses.UserBid;

import java.util.List;

/**
 * Created by kkallepalli on 6/26/2016.
 */
public class ReviewAdapter extends BaseAdapter {

    private String TAG = "ReviewAdapter";
    private final Context context;
    private List<Review> reviews;

    public ReviewAdapter(Context context, List<Review> reviewList) {
        this.context = context;
        this.reviews =reviewList;
    }


    @Override
    public int getCount() {
        return reviews.size();
    }

    @Override
    public Review getItem(int position) {
        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Review review=reviews.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.review_list, null);

        TextView desc=(TextView)rowView.findViewById(R.id.reviewDesc);
        desc.setText(review.getDesc());

        TextView reviewer=(TextView)rowView.findViewById(R.id.reviewerName);
        reviewer.setText(review.getReviewer());

        return rowView;
    }
}
