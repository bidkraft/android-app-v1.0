package com.handson.bidkraft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.handson.bidkraft.R;
import com.handson.bidkraft.entities.Job;
import com.handson.bidkraft.entities.Review;

import java.util.List;

/**
 * Created by krishmani on 7/1/2016.
 */
public class JobAdapter extends BaseAdapter {

    private String TAG = "ReviewAdapter";
    private final Context context;
    private List<Job> jobs;

    public JobAdapter(Context context, List<Job> jobs) {
        this.context = context;
        this.jobs = jobs;
    }

    @Override
    public int getCount() {
        return jobs.size();
    }

    @Override
    public Job getItem(int position) {
        return jobs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Job job=jobs.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.review_list, null);

        return rowView;
    }
}
