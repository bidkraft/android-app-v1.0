package com.handson.bidkraft.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.handson.bidkraft.R;
import com.handson.bidkraft.activities.AcceptBidActivity;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.entities.Review;
import com.handson.bidkraft.model.UserRequest;
import com.handson.bidkraft.responses.UserBid;
import android.view.ViewGroup.LayoutParams;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kkallepalli on 4/28/2016.
 */
public class MyRequestBidAdapter extends BaseAdapter {

    private String TAG = "MyRequestBidAdapter";
    private final Context context;
    private List<UserBid> bidList;
    private UserRequest request;

    public MyRequestBidAdapter(Context context, List<UserBid> bidList, UserRequest request) {
        this.context = context;
        this.bidList = bidList;
        this.request =request;
    }

    @Override
    public int getCount() {
        System.out.println("Count of List in Adapter "+bidList.size());
        return bidList.size();
    }

    @Override
    public UserBid getItem(int position) {
        return bidList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final UserBid data = bidList.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.req_bid_list, null);

        TextView biddesc = (TextView) rowView.findViewById(R.id.myreqbiddesc);
        TextView bidusername = (TextView) rowView.findViewById(R.id.myreqbidderName);
        TextView acceptBid=(TextView)rowView.findViewById(R.id.acceptBid);
        TextView bidderRating=(TextView)rowView.findViewById(R.id.bidderRating);
        ImageView userpic=(ImageView)rowView.findViewById(R.id.bidUserPic);

        biddesc.setText(data.getDescription());
        bidusername.setText(""+data.getUserid());
        if(data.getUser()!=null) {
            bidderRating.setText(data.getUser().getRating());
        }

        userpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popup=inflater.inflate(R.layout.reviewspopup,null);

                ListView reviews=(ListView)popup.findViewById(R.id.userReviews);
                List<Review> reviewsResponse=new ArrayList<Review>();

                //for testing,remove after service
                Review review=new Review();
                review.setReviewer("John");
                review.setDesc("I'm happy with the baby sitting service provided by him");
                reviewsResponse.add(review);

                Review review1=new Review();
                review1.setReviewer("Smith");
                review1.setDesc("He is very good care taker");
                reviewsResponse.add(review1);

                Review review2=new Review();
                review2.setReviewer("Grant");
                review2.setDesc("My kids are very happy with him");
                reviewsResponse.add(review2);

                ReviewAdapter reviewAdapter=new ReviewAdapter(context,reviewsResponse);
                reviews.setAdapter(reviewAdapter);
                final PopupWindow popupWindow = new PopupWindow(popup, LayoutParams.MATCH_PARENT,800);
                popupWindow.setFocusable(true);
                popupWindow.update();
                popupWindow.showAtLocation(rowView, Gravity.CENTER,1,1);
            }
        });


        acceptBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(rowView.getContext())
                        .setTitle("Confirm")
                        .setMessage("Do you really want to accept this bid?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent intent=new Intent(context,AcceptBidActivity.class);
                                intent.putExtra("bidinfo",data);
                                intent.putExtra("reqinfo",request);
                                context.startActivity(intent);
                                //Toast.makeText(rowView.getContext(), ""+data.getBidid(), Toast.LENGTH_SHORT).show();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
//        reqDuration.setText(data.getDescription());
//        if(data.getStatus().equals("CONFIRM"))
//        {
//            status.setImageResource(R.drawable.thumbup);
//        }
//        else if(data.getStatus().equals("REJECTED"))
//        {
//            status.setImageResource(R.drawable.thumbdown);
//        }
//        else
//        {
//            status.setImageResource(R.drawable.thumbsupdown);
//        }

        return rowView;
    }

    class AcceptBidTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(context);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("Response from Url: " + result);
                dialog.cancel();
                Toast.makeText(context,"Accept Bid!!",Toast.LENGTH_SHORT).show();
//                Intent intent=new Intent(context,NavHomeActivity.class);
//                startActivity(intent);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }
    }
}
