package com.handson.bidkraft.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.model.UserRequest;

import java.util.List;

/**
 * Created by kkallepalli on 4/5/2016.
 */
public class MyRequestAdapter extends BaseAdapter {
    private String TAG = "MyRequestAdapter";
    private final Context context;
    private List<UserRequest> requestList;

    public MyRequestAdapter(Context context, List<UserRequest> requestList) {
        this.context = context;
        this.requestList = requestList;
    }

    @Override
    public int getCount() {
        return requestList.size();
    }

    @Override
    public UserRequest getItem(int position) {
        return requestList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserRequest data = requestList.get(position);
        //Log.v(TAG, position + data.getTitle() + " " + data.getSurveyEnrollment());
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.request_list, null);

        TextView reqListTitle = (TextView) rowView.findViewById(R.id.reqListTitle);
        TextView reqDesc = (TextView) rowView.findViewById(R.id.reqListDesc);
        TextView reqListDate = (TextView) rowView.findViewById(R.id.reqListDate);
        TextView totalBids = (TextView) rowView.findViewById(R.id.totalBids);

        System.out.println("Req Title in list:"+data.getRequest().getTitle());
        reqListTitle.setText(data.getRequest().getTitle());
        reqListDate.setText(data.getRequest().getStart_date()+" to "+ data.getRequest().getEnd_date());
        reqDesc.setText(data.getRequest().getDescription());
        if(data.getBids()!=null)
        {
            totalBids.setText(""+data.getBids().size());
        }
        else
        {
            totalBids.setText("0");
        }

        return rowView;
    }
}
