package com.handson.bidkraft.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.model.UserRequest;

import java.util.List;

/**
 * Created by kkallepalli on 4/12/2016.
 */
public class MyBidAdapter extends BaseAdapter {
    private String TAG = "MyRequestAdapter";
    private final Context context;
    private List<UserRequest> bidList;

    public MyBidAdapter(Context context, List<UserRequest> bidList) {
        this.context = context;
        this.bidList = bidList;
    }

    @Override
    public int getCount() {
        return bidList.size();
    }

    @Override
    public UserRequest getItem(int position) {
        return bidList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserRequest data = bidList.get(position);
        //Log.v(TAG, position + data.getTitle() + " " + data.getSurveyEnrollment());
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.bid_list, null);

        System.out.println("List Child bidid:"+data.getBids().get(position).getBidid());

        TextView bidId = (TextView) rowView.findViewById(R.id.bidTitle);
        TextView reqId = (TextView) rowView.findViewById(R.id.bidDesc);
        TextView bidAmount = (TextView) rowView.findViewById(R.id.bidAmount);
        ImageView status=(ImageView)rowView.findViewById(R.id.bidStatus);

        bidId.setText(""+data.getBids().get(position).getBidid());
        reqId.setText(data.getBids().get(position).getDescription());
        bidAmount.setText(""+data.getBids().get(position).getAmount());
        if(data.getBids().get(position).getStatus().equals("CONFIRM"))
        {
            status.setImageResource(R.drawable.thumbup);
        }
        else if(data.getBids().get(position).getStatus().equals("REJECTED"))
        {
            status.setImageResource(R.drawable.thumbdown);
        }
        else
        {
            status.setImageResource(R.drawable.thumbsupdown);
        }

        return rowView;
    }
}