package com.handson.bidkraft.responses;

import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.responses.UserRequestResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by kkallepalli on 4/2/2016.
 */
public class GetAvailableResponse {

    private String status;
    private List<String> keys;
    private Map<String,List<UserRequestResponse>> entities;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public Map<String, List<UserRequestResponse>> getEntities() {
        return entities;
    }

    public void setEntities(Map<String, List<UserRequestResponse>> entities) {
        this.entities = entities;
    }
}
