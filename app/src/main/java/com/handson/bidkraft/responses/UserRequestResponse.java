package com.handson.bidkraft.responses;

import com.handson.bidkraft.entities.Job;
import com.handson.bidkraft.entities.Request;

import java.io.Serializable;
import java.util.List;

public class UserRequestResponse implements Serializable{

	public Request request;
	public List<UserBid> bids;
	private Job job;

	public UserRequestResponse() {
	}

	public UserRequestResponse(Request request, List<UserBid> bids) {
		this.request = request;
		this.bids = bids;
	}

	public void setRequest(Request request) {
		this.request = request;
	}
	public void setBids(List<UserBid> bids) {
		this.bids = bids;
	}

	public Request getRequest() {
		return request;
	}

	public List<UserBid> getBids() {
		return bids;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
}
