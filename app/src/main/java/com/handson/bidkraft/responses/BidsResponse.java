package com.handson.bidkraft.responses;

import java.io.Serializable;
import java.util.List;

public class BidsResponse implements Serializable {

	public long requestid;
	public int totalbids;
	public List<UserBid> bids;
	
	public BidsResponse() {
		
	}

	public BidsResponse(long requestid, int totalbids, List<UserBid> bids) {
		this.requestid = requestid;
		this.totalbids = totalbids;
		this.bids = bids;
	}

	public long getRequestid() {
		return requestid;
	}

	public void setRequestid(long requestid) {
		this.requestid = requestid;
	}

	public int getTotalbids() {
		return totalbids;
	}

	public void setTotalbids(int totalbids) {
		this.totalbids = totalbids;
	}

	public List<UserBid> getBids() {
		return bids;
	}

	public void setBids(List<UserBid> bids) {
		this.bids = bids;
	}
}
