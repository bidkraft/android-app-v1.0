package com.handson.bidkraft.responses;

import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.User;

import java.io.Serializable;

public class UserBid extends Bid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	
	public User getUser() {
		return this.user;
	}
}
