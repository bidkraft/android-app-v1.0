package com.handson.bidkraft.application;

import android.app.Application;

import com.handson.bidkraft.entities.User;

/**
 * Created by kkallepalli on 4/5/2016.
 */
public class BidkraftApplication extends Application {

    private User user;

    private String address;
    private String city;
    private String zipcode;
    private double lat;
    private double lon;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
