package com.handson.bidkraft.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.Request;

public class AcceptBidActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_bid);

        setTitle("Job - Summary");

        final BidkraftApplication  application=(BidkraftApplication)getApplication();

        TextView bidConfEmail=(TextView)findViewById(R.id.bidConfEmail);
        bidConfEmail.setText(application.getUser().getEmail());

        Bid bid=(Bid)getIntent().getSerializableExtra("bidinfo");
        Request request=(Request) getIntent().getSerializableExtra("reqinfo");

        TextView textView=(TextView)findViewById(R.id.jobSummary);
        //textView.setText(bid.getDescription());
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(getApplicationContext(),NavHomeActivity.class);
        startActivity(intent);
    }
}
