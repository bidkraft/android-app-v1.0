package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.model.LoginResponse;
import com.handson.bidkraft.utility.JsonWebServiceCaller;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private EditText fname;
    private EditText lname;
    private EditText email;
    private EditText address;
    private EditText password;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        fname=(EditText)findViewById(R.id.fname);
        lname=(EditText)findViewById(R.id.lname);
        email=(EditText)findViewById(R.id.regEmail);
        address=(EditText)findViewById(R.id.regAddress);
        password=(EditText)findViewById(R.id.regPwd);
        submit=(Button) findViewById(R.id.regSbmtBtn);

        LocationManager locationManager=(LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        Criteria criteria=new Criteria();
        String provider=locationManager.getBestProvider(criteria,true);
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return ;
        }
        final Location myLocation=locationManager.getLastKnownLocation(provider);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Registration clicked ");
                new RegistrationTask().execute(email.getText().toString(),password.getText().toString(),""+myLocation.getLatitude(),""+myLocation.getLongitude(),"","");
            }
        });


    }

    class RegistrationTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(RegistrationActivity.this);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.REGISTRATION);
            Map<String,String> keys=new HashMap<>();
            keys.put("email",params[0]);
            keys.put("password",params[1]);
            keys.put("lat",params[2]);
            keys.put("lon",params[3]);
            keys.put("bgc_status",params[4]);
            keys.put("rating",params[5]);
            request.getEntities().put(ServiceKeys.REGISTRATION, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.REGISTRATION, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("Response from Url: " + result);
                LoginResponse loginResponse = mapper.readValue(result, LoginResponse.class);
                dialog.cancel();
                Toast.makeText(getApplicationContext(),"registration called",Toast.LENGTH_LONG).show();
                if(loginResponse.getStatus().equals("success"))
                {
                    Toast.makeText(RegistrationActivity.this,"Registration Successful",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(RegistrationActivity.this,"Registration failed",Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }


}
