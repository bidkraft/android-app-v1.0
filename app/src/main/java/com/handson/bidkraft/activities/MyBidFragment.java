package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.adapter.MyBidAdapter;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.model.MyBidResponse;
import com.handson.bidkraft.model.UserRequest;
import com.handson.bidkraft.utility.JsonWebServiceCaller;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MyBidFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private View myFragmentView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MyBidFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MyBidFragment newInstance(int columnCount) {
        MyBidFragment fragment = new MyBidFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView= inflater.inflate(R.layout.fragment_my_bid, container, false);
        BidkraftApplication application=(BidkraftApplication)getActivity().getApplication();
        System.out.println("Access Application User object:" + application.getUser().getUserid());
        new RequestTask().execute("" + application.getUser().getUserid());
        return  myFragmentView;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class RequestTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(getActivity());
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.GETUSERBIDS);
            Map<String,String> keys=new HashMap<>();
            keys.put("userid",params[0]);
            request.getEntities().put(ServiceKeys.GETUSERBIDS, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.GETUSERBIDS, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("My Bids Response from Url: " + result);
                MyBidResponse reqResponse = mapper.readValue(result, MyBidResponse.class);
                dialog.cancel();
                List<UserRequest> bids=reqResponse.getEntities().get("GETUSERBID").get("acceptedBids");
                MyBidAdapter myBidAdapter=new MyBidAdapter(getView().getContext(),bids);
                ListView listView=(ListView) getView().findViewById(R.id.myBidList);
                listView.setAdapter(myBidAdapter);
                dialog.cancel();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }
}
