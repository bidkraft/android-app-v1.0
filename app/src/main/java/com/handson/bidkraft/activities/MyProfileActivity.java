package com.handson.bidkraft.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;

public class MyProfileActivity extends AppCompatActivity {


    RatingBar ratingBar;
    TextView pname;
    TextView email;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        final BidkraftApplication application=(BidkraftApplication)getApplication();
        ratingBar = (RatingBar) findViewById(R.id.myRating);
        pname = (TextView) findViewById(R.id.profName);
        email = (TextView) findViewById(R.id.myEmail);


        ratingBar.setRating(application.getUser().getRating());
        pname.setText(""+application.getUser().getUserid());
        email.setText(application.getUser().getEmail());

        map = ((com.google.android.gms.maps.MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        map.setMyLocationEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setAllGesturesEnabled(false);

        LatLng currentLocation=new LatLng(application.getLat(),application.getLon());
        map.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        map.animateCamera(CameraUpdateFactory.zoomTo(11));
        map.addMarker(new MarkerOptions().position(currentLocation).title("Default Address"));
    }
}
