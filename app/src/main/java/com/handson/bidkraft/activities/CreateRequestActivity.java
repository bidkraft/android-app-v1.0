package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.model.Entity;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.model.LoginResponse;
import com.handson.bidkraft.model.LuisResponse;
import com.handson.bidkraft.utility.DatePickerFragment;
import com.handson.bidkraft.utility.JsonWebServiceCaller;
import com.microsoft.projectoxford.speechrecognition.DataRecognitionClient;
import com.microsoft.projectoxford.speechrecognition.ISpeechRecognitionServerEvents;
import com.microsoft.projectoxford.speechrecognition.MicrophoneRecognitionClient;
import com.microsoft.projectoxford.speechrecognition.RecognitionResult;
import com.microsoft.projectoxford.speechrecognition.RecognitionStatus;
import com.microsoft.projectoxford.speechrecognition.SpeechRecognitionMode;
import com.microsoft.projectoxford.speechrecognition.SpeechRecognitionServiceFactory;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CreateRequestActivity extends AppCompatActivity  implements ISpeechRecognitionServerEvents{

    public int m_waitSeconds = 0;
    public DataRecognitionClient dataClient = null;
    public MicrophoneRecognitionClient micClient = null;
    public FinalResponseStatus isReceivedResponse = FinalResponseStatus.NotReceived;

    public enum FinalResponseStatus { NotReceived, OK, Timeout }

    ImageView stDate;
    ImageView endDate;
    TextView reqStDate;
    TextView reqEndDate;
    EditText title;
    EditText desc;
    EditText amt;
    EditText address;
    EditText zipcode;
    AutoCompleteTextView city;
    LinearLayout mylayout;

    Button save;
    Button cancel;

    GoogleMap map;

    String[] cities={"Norfolk VA","Richmond VA","Charlotte NC","Hampton VA","Williamsburg VA","Sanjose CA","Newyork NY"};

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.speak_luis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.luisMic) {
            startLuis();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_request);

        stDate=(ImageView)findViewById(R.id.reqStDtPicker);
        endDate=(ImageView)findViewById(R.id.reqEndDtPicker);
        reqStDate=(TextView) findViewById(R.id.reqStDt);
        reqEndDate=(TextView)findViewById(R.id.reqEndDt);

        save=(Button)findViewById(R.id.createReqSave);
        cancel=(Button)findViewById(R.id.createReqCancel);
        title=(EditText)findViewById(R.id.reqTitle);
        desc=(EditText)findViewById(R.id.reqDesc);
        amt=(EditText)findViewById(R.id.reqAmt);
        address=(EditText)findViewById(R.id.createReqAddr);
        city=(AutoCompleteTextView)findViewById(R.id.createReqCity);
        mylayout =(LinearLayout)findViewById(R.id.createReqLayout);
        zipcode =(EditText) findViewById(R.id.createReqZip);

        ArrayAdapter<String> cityAdapter=new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,cities);

        city.setThreshold(3);
        city.setAdapter(cityAdapter);

        map = ((com.google.android.gms.maps.MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();

        final BidkraftApplication application=(BidkraftApplication) getApplicationContext();

        address.setText(application.getAddress());
        city.setText(application.getCity());
        zipcode.setText(application.getZipcode());


        map.setMyLocationEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setAllGesturesEnabled(false);

        LatLng currentLocation=new LatLng(application.getLat(),application.getLon());
        map.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        map.animateCamera(CameraUpdateFactory.zoomTo(14));
        map.addMarker(new MarkerOptions().position(currentLocation).title("You are here!"));

        stDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("DATE",1);
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setArguments(bundle);
                newFragment.show(getFragmentManager(),"StartDatePicker");
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("DATE",2);
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setArguments(bundle);
                newFragment.show(getFragmentManager(),"StartDatePicker");
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent=new Intent(CreateRequestActivity.this,NavHomeActivity.class);
                startActivity(intent);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView startDate = (TextView)findViewById(R.id.reqStDt);
                TextView endDate = (TextView)findViewById(R.id.reqEndDt);
                new CreateRequestTask().execute(title.getText().toString(),desc.getText().toString(),startDate.getText().toString()
                        ,endDate.getText().toString(),"36.889251","-76.301211",address.getText().toString());
            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if ( Build.VERSION.SDK_INT >= 23 &&
                        ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return ;
                }

                map.setMyLocationEnabled(true);
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.getUiSettings().setAllGesturesEnabled(false);

                LatLng currentLocation=new LatLng(application.getLat(),application.getLon());
                map.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

                map.animateCamera(CameraUpdateFactory.zoomTo(14));
                map.addMarker(new MarkerOptions().position(currentLocation).title("You are here!"));

            }
        });

        setTitle("Create Request");


    }

    public void startLuis()
    {
        m_waitSeconds = getMode() == SpeechRecognitionMode.ShortPhrase ? 20 : 200;

        if (getUseMicrophone()) {
            if (micClient == null) {
                // may change in future based on speech
                if (true) {
                    micClient = SpeechRecognitionServiceFactory.createMicrophoneClientWithIntent(
                            CreateRequestActivity.this,
                            getDefaultLocale(),
                            CreateRequestActivity.this,
                            getPrimaryKey(),
                            getSecondaryKey(),
                            getLuisAppId(),
                            getLuisSubscriptionID());
                }
                else
                {
                    micClient = SpeechRecognitionServiceFactory.createMicrophoneClient(
                            CreateRequestActivity.this ,
                            getMode(),
                            getDefaultLocale(),
                            CreateRequestActivity.this,
                            getPrimaryKey(),
                            getSecondaryKey());
                }
            }

            micClient.startMicAndRecognition();
        }
    }

    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(),"DatePicker");
       // startDate.setText(newFragment.year);
    }

    public String getPrimaryKey() {
        return this.getString(R.string.primaryKey);
    }

    public String getSecondaryKey() {
        return this.getString(R.string.secondaryKey);
    }

    private String getLuisAppId() {
        return this.getString(R.string.luisAppID);
    }

    private String getLuisSubscriptionID() {
        return this.getString(R.string.luisSubsID);
    }

    private String getDefaultLocale() {
        return "en-us";
    }

    private SpeechRecognitionMode getMode() {
        return SpeechRecognitionMode.LongDictation;
    }

    private Boolean getWantIntent() {
        return  Boolean.TRUE;
    }


    private Boolean getUseMicrophone() {
        return Boolean.TRUE;
    }

    @Override
    public void onPartialResponseReceived(String s) {
        //Toast.makeText(getApplicationContext(),"Partial Response:"+s,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFinalResponseReceived(RecognitionResult response) {
        boolean isFinalDicationMessage = this.getMode() == SpeechRecognitionMode.LongDictation &&
                (response.RecognitionStatus == RecognitionStatus.EndOfDictation ||
                        response.RecognitionStatus == RecognitionStatus.DictationEndSilenceTimeout);
        if (null != this.micClient && this.getUseMicrophone() && ((this.getMode() == SpeechRecognitionMode.ShortPhrase) || isFinalDicationMessage)) {
            // we got the final result, so it we can end the mic reco.  No need to do this
            // for dataReco, since we already called endAudio() on it as soon as we were done
            // sending all the data.
            this.micClient.endMicAndRecognition();
        }

        if (isFinalDicationMessage) {
            this.isReceivedResponse = FinalResponseStatus.OK;
        }

        if (!isFinalDicationMessage) {
            for (int i = 0; i < response.Results.length; i++) {
//                Toast.makeText(getApplicationContext(),"[" + i + "]" + " Confidence=" + response.Results[i].Confidence +
//                        " Text=\"" + response.Results[i].DisplayText + "\"",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onIntentReceived(String result) {
        ObjectMapper objectMapper=new ObjectMapper();
        LuisResponse luisResponse=null;
        try {
            luisResponse=(LuisResponse)objectMapper.readValue(result,LuisResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(luisResponse!=null) {
            Log.i("Luis", result);
            if(luisResponse.getIntents().get(0).getIntent().equalsIgnoreCase("category_baby_sit")) {
                title.setText("Baby Sitter");
            }
            desc.setText(luisResponse.getQuery());
            for (Entity entity:luisResponse.getEntities())
            {
                LinkedHashMap jsonObject=(LinkedHashMap)entity.getResolution();
                if(entity.type.equalsIgnoreCase("builtin.datetime.date")) {
                    if(jsonObject.containsKey("date")) {
                        try {
                            reqStDate.setText(jsonObject.get("date").toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(entity.type.equalsIgnoreCase("builtin.age"))
                {
                    TextView textView = new TextView(getApplicationContext());
                    textView.setText("Age");
                    textView.setTextColor(Color.parseColor("#000000"));
                    mylayout.addView(textView);

                    EditText editText = new EditText(getApplicationContext());
                    editText.setText(entity.getEntity());
                    editText.setTextColor(Color.parseColor("#000000"));
                    mylayout.addView(editText);
                }
                else {
                    TextView textView = new TextView(getApplicationContext());
                    textView.setText(entity.getType());
                    textView.setTextColor(Color.parseColor("#000000"));
                    mylayout.addView(textView);

                    EditText editText = new EditText(getApplicationContext());
                    editText.setText(entity.getEntity());
                    editText.setTextColor(Color.parseColor("#000000"));
                    mylayout.addView(editText);
                }
            }
        }
        else
        {
            title.setText(result);
        }
       //Toast.makeText(getApplicationContext(),"Intent recieved:"+result,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(int i, String s) {
        Toast.makeText(getApplicationContext(),"error:"+s,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAudioEvent(boolean recording) {
        if (recording) {
            Toast.makeText(getApplicationContext(),"Please start speaking",Toast.LENGTH_LONG).show();
        }

        if (!recording) {
            this.micClient.endMicAndRecognition();
        }

    }

    class CreateRequestTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(CreateRequestActivity.this);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.CREQ);
            Map<String,String> keys=new HashMap<>();
            final BidkraftApplication application= (BidkraftApplication) getApplicationContext();
            keys.put("userid",""+application.getUser().getUserid());
            keys.put("title",params[0]);
            keys.put("description",params[1]);
            keys.put("start_date",params[2]+" 12:00:00");
            keys.put("end_date",params[3]+" 12:00:00");
            keys.put("lat",params[4]);
            keys.put("lon",params[5]);
            keys.put("address",params[6]);
            request.getEntities().put(ServiceKeys.CREQ, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.CREATEREQUEST, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("Response from Url: " + result);
                dialog.cancel();
                Toast.makeText(getApplicationContext(),"Request Created Succesfully!!",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),NavHomeActivity.class);
                startActivity(intent);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }

}
