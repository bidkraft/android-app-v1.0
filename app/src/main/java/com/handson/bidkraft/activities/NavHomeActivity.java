package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.Algorithm;
import com.google.maps.android.clustering.algo.NonHierarchicalDistanceBasedAlgorithm;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.entities.Filter;
import com.handson.bidkraft.entities.GeoQuery;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.responses.GetAvailableResponse;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.responses.UserRequestResponse;
import com.handson.bidkraft.utility.JsonWebServiceCaller;
import com.handson.bidkraft.utility.KraftMarker;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavHomeActivity extends AppCompatActivity implements OnMapReadyCallback {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private  GoogleMap map;
    private static List<UserRequestResponse> requests;
    private FloatingActionButton actionButton;
    private FloatingActionButton profileBtn;
    private ClusterManager<KraftMarker> clusterManager;
    private KraftMarker clickedMarker;
    private Algorithm<KraftMarker> clusteMarkerAlgorithm;
    private List<KraftMarker> markerData=new ArrayList<KraftMarker>();
    private  TabLayout tabLayout;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_home);

       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        actionButton=(FloatingActionButton)findViewById(R.id.createReqBtn);
        profileBtn =(FloatingActionButton)findViewById(R.id.showProfileBtn);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout= (TabLayout)findViewById(R.id.tabs);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.homeicon);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_wrench_white_18dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_wrench_white_18dp);
        tabLayout.getTabAt(3).setIcon(R.drawable.settings);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NavHomeActivity.this,CreateRequestActivity.class);
                startActivity(intent);
            }
        });

        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NavHomeActivity.this,MyProfileActivity.class);
                startActivity(intent);
            }
        });




        SearchView view = (SearchView)findViewById(R.id.mainSearch);
        view.setVisibility(View.VISIBLE);
        view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Toast.makeText(NavHomeActivity.this, query+":"+clusteMarkerAlgorithm.getItems().size(), Toast.LENGTH_SHORT).show();
                markerData.addAll(clusteMarkerAlgorithm.getItems());
                for (KraftMarker marker:clusteMarkerAlgorithm.getItems())
                {
                    if(!marker.getTitle().toLowerCase().contains(query.toLowerCase())) {
                        clusterManager.removeItem(marker);
                    }
                }
                clusterManager.cluster();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        view.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //Toast.makeText(getApplicationContext(),"search close"+clusteMarkerAlgorithm.getItems().size(),Toast.LENGTH_LONG).show();
                for (KraftMarker marker:markerData)
                {
                    clusterManager.addItem(marker);
                }
                clusterManager.cluster();
                return false;
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                SearchView view = (SearchView)findViewById(R.id.mainSearch);
                if(position==0)
                {
                    actionButton.setVisibility(View.VISIBLE);
                    profileBtn.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);
                }
                else
                {
                    actionButton.setVisibility(View.INVISIBLE);
                    profileBtn.setVisibility(View.INVISIBLE);
                    profileBtn.setVisibility(View.INVISIBLE);
                    view.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setMessage("Are you sure you want to exit app?")
                .setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nav_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

        LocationManager locationManager=(LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        Criteria criteria=new Criteria();
        String provider=locationManager.getBestProvider(criteria,true);
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return ;
        }

        map.setMyLocationEnabled(true);
        Location myLocation=locationManager.getLastKnownLocation(provider);
        if(myLocation==null)
        {
            myLocation=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            System.out.println("mylocation null");
        }
        if(myLocation != null) {
            System.out.println("Current Lat:" + myLocation.getLatitude());
        }
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.info_window, null);
                System.out.println("Title:"+marker.getTitle());
                //Request markerInfo=requests.get(Integer.parseInt(marker.getSnippet()));
                if(clickedMarker==null) {
                }
                else
                {
                    UserRequestResponse markerInfo=requests.get(Integer.parseInt(clickedMarker.getSnippet()));

                    TextView title = (TextView) v.findViewById(R.id.infoTitle);
                    title.setText(markerInfo.getRequest().getTitle());

                    TextView address = (TextView) v.findViewById(R.id.infoAddress);
                    address.setText(markerInfo.getRequest().getAddress());

                }

                return v;
            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                System.out.println("Snipet for Info:"+clickedMarker.getSnippet());
                Intent intent=new Intent(getApplicationContext(),RequestInfoActivity.class);
                intent.putExtra("reqInfo",requests.get(Integer.parseInt(clickedMarker.getSnippet())));
                startActivity(intent);
            }
        });

        final BidkraftApplication application= (BidkraftApplication) getApplicationContext();
        application.setLat(myLocation.getLatitude());
        application.setLon(myLocation.getLongitude());

        LatLng currentLocation=new LatLng(myLocation.getLatitude(),myLocation.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        map.animateCamera(CameraUpdateFactory.zoomTo(6));
        //map.addMarker(new MarkerOptions().position(currentLocation).title("You are here!"));

        clusterManager=new ClusterManager<KraftMarker>(this,map);
        map.setOnCameraChangeListener(clusterManager);
        map.setOnMarkerClickListener(clusterManager);

        clusteMarkerAlgorithm=new NonHierarchicalDistanceBasedAlgorithm<>();
        clusterManager.setAlgorithm(clusteMarkerAlgorithm);
        clusterManager.setRenderer(new KraftMarkerRenderer());

        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<KraftMarker>() {
            @Override
            public boolean onClusterClick(Cluster<KraftMarker> cluster) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        cluster.getPosition(), (float) Math.floor(map
                                .getCameraPosition().zoom + 3)), 300,
                        null);
                return true;
            }
        });
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<KraftMarker>()
        {
            @Override
            public boolean onClusterItemClick(KraftMarker item) {
                System.out.println("Clicked Snipet:"+ item.getTitle()+":"+ item.getSnippet());
                clickedMarker = item;
                return false;
            }
        });

        new RequestTask().execute();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_nav_home, container, false);
            /*TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));*/
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            System.out.println("Position of fragment "+position);
            Fragment fragment=null;
            if(position==0)
            {
                SupportMapFragment supportMapFragment=new SupportMapFragment();
                supportMapFragment.getMapAsync(NavHomeActivity.this);
                fragment=supportMapFragment;
            }
            else if(position==1) {
                fragment=new MyRequestFragment();
                //fragment=new MyKraftFragment();
            }
            else if(position==2) {
                fragment=new MyBidFragment();
            }
            else if(position==3){
                fragment=new SettingsFragment();
            }
            //return PlaceholderFragment.newInstance(position + 1);
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Home";
                case 1:
                    return "Reqs";
                case 2:
                    return "Bids";
                case 3:
                    return "Settings";
            }
            return null;
        }
    }

    class KraftMarkerRenderer extends DefaultClusterRenderer<KraftMarker>
    {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        public KraftMarkerRenderer() {
            super(getApplicationContext(), map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(KraftMarker marker, MarkerOptions markerOptions) {
            final BidkraftApplication application=(BidkraftApplication)getApplicationContext();
            if(requests.get(Integer.parseInt(marker.getSnippet())).getRequest().getUserid()==application.getUser().getUserid())
            {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            }
            else
            {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
        }
    }

    class RequestTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(NavHomeActivity.this);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.GETAVAILABLEREQUESTS);
            Map<String,Filter<GeoQuery>> keys=new HashMap<>();

            Filter<GeoQuery> filter=new Filter<>();
            filter.setOperator("and");
            filter.setType("group");

            //GeoQuery query=new GeoQuery("50","-121.9433009","37.4144626","geo");
            GeoQuery query=new GeoQuery("50","-76.301211","36.889251","geo");
            filter.getQueries().add(query);

            keys.put("filter",filter);

            request.getEntities().put(ServiceKeys.GETAVAILABLEREQUESTS, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.LOGIN, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                GetAvailableResponse reqResponse = mapper.readValue(result, GetAvailableResponse.class);
                dialog.cancel();
                requests = reqResponse.getEntities().get("GETAVAILABLEREQ");
                System.out.println("Response from GET Available Requests Url: " + requests.size());
                for(int i=0;i<requests.size();i++)
                {
//                    LatLng location=new LatLng(requests.get(i).getLatitude_request(),requests.get(i).getLongitude_request());
//                    MarkerOptions markerOptions=new MarkerOptions()
//                           .position(location)
//                           .title(requests.get(i).getTitle())
//                            .snippet(""+i)
//                            .alpha(0.7f)
//                           .infoWindowAnchor(0.5f,1.0f)
//                           .icon(BitmapDescriptorFactory.fromResource(R.drawable.loc_icon));
//
//                    Marker marker=map.addMarker(markerOptions);
//                    marker.hideInfoWindow();
//                    dropPinEffect(marker);
                    clusterManager.addItem(new KraftMarker(requests.get(i).getRequest().getLat(),requests.get(i).getRequest().getLon(),requests.get(i).getRequest().getTitle(),""+i));
                }

                clusterManager.cluster();

            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }

    private void dropPinEffect(final Marker marker) {
        // Handler allows us to repeat a code block after a specified delay
        final android.os.Handler handler = new android.os.Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;

        // Use the bounce interpolator
        final android.view.animation.Interpolator interpolator =
                new BounceInterpolator();

        // Animate marker with a bounce updating its position every 15ms
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                // Calculate t for bounce based on elapsed time
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                // Set the anchor
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    // Post this event again 15ms from now.
                    handler.postDelayed(this, 15);
                } else { // done elapsing, show window
                    marker.showInfoWindow();
                }
            }
        });
    }
}
