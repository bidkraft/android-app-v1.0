package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.os.AsyncTask;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.adapter.MyRequestBidAdapter;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.model.MyBidResponse;
import com.handson.bidkraft.model.MyRequestBidsResponse;
import com.handson.bidkraft.model.UserRequest;
import com.handson.bidkraft.responses.UserBid;
import com.handson.bidkraft.utility.JsonWebServiceCaller;

import org.codehaus.jackson.map.ObjectMapper;
import org.lucasr.twowayview.TwoWayView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestDetailsActivity extends AppCompatActivity implements GestureOverlayView.OnGestureListener {

    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    UserRequest request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);

        Intent intent=getIntent();
        request=(UserRequest) intent.getSerializableExtra("requestDetails");

        TextView textView=(TextView)findViewById(R.id.reqDetTitle);
        textView.setText(request.getRequest().getTitle());

        TextView textDesc=(TextView)findViewById(R.id.reqDetDesc);
        textDesc.setText(request.getRequest().getDescription());

        TextView stDate=(TextView)findViewById(R.id.reqDetStDate);
        stDate.setText(request.getRequest().getStart_date().toString());

        TextView endDate=(TextView)findViewById(R.id.reqDetEndDate);
        endDate.setText(request.getRequest().getEnd_date().toString());

        TextView address=(TextView)findViewById(R.id.reqDetAddress);
        address.setText(request.getRequest().getAddress());

        BidkraftApplication application=(BidkraftApplication)getApplication();

        MyRequestBidAdapter myReqBidAdapter=new MyRequestBidAdapter(RequestDetailsActivity.this,request.getBids(),request);

        ListView listView=(ListView)findViewById(R.id.myReqBidList);
        listView.setAdapter(myReqBidAdapter);
        myReqBidAdapter.notifyDataSetChanged();

        //new RequestTask().execute("" + request.getRequest().getRequestid());

    }

    @Override
    public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {

    }

    @Override
    public void onGesture(GestureOverlayView overlay, MotionEvent event) {

    }

    @Override
    public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {

    }

    @Override
    public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {

    }

    class MyResponse
    {
        List<Bid> bids;
        int totalbids;

        public MyResponse() {
        }

        public MyResponse(List<Bid> bids, int totalbids) {
            this.bids = bids;
            this.totalbids = totalbids;
        }

        public List<Bid> getBids() {
            return bids;
        }

        public void setBids(List<Bid> bids) {
            this.bids = bids;
        }

        public int getTotalbids() {
            return totalbids;
        }

        public void setTotalbids(int totalbids) {
            this.totalbids = totalbids;
        }
    }

    class RequestTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(RequestDetailsActivity.this);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.GETREQUESTBIDS);
            Map<String,String> keys=new HashMap<>();
            keys.put("reqid",params[0]);
            request.getEntities().put(ServiceKeys.GETREQUESTBIDS, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.GETREQBIDS, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("My Bids Response from Url: " + result);
                MyRequestBidsResponse reqResponse = mapper.readValue(result, MyRequestBidsResponse.class);
                dialog.cancel();
                List<UserBid> bids=reqResponse.getEntities().get(ServiceKeys.GETREQUESTBIDS).getBids();
                //MyResponse response=mapper.readValue("{\"bids\":[{\"bidid\":1,\"description\":\"will be available by 5:00pm\",\"amount\":20.0,\"currency\":0,\"status\":\"CONFIRM\",\"user\":{\"userid\":1,\"email\":\"kkall002@odu.edu\",\"bgcstatus\":\"INITIALIZED\",\"rating\":3.0,\"image\":\"\",\"lat\":59.8802,\"lon\":56.8898},\"requestid\":1,\"userid\":1,\"biddate\":null},{\"bidid\":5,\"description\":\"will be available by 5:00pm\",\"amount\":10.0,\"currency\":0,\"status\":\"REJECTED\",\"user\":{\"userid\":3,\"email\":\"saikaranbalmuri@gmail.com\",\"bgcstatus\":\"INITIALIZED\",\"rating\":5.0,\"image\":\"\",\"lat\":50966.0,\"lon\":50677.0},\"requestid\":1,\"userid\":3,\"biddate\":null},{\"bidid\":4,\"description\":\"will be available by 5:00pm\",\"amount\":30.0,\"currency\":0,\"status\":\"REJECTED\",\"user\":{\"userid\":2,\"email\":\"sb@gmail.com\",\"bgcstatus\":\"INITIALIZED\",\"rating\":5.0,\"image\":\"\",\"lat\":50966.0,\"lon\":50677.0},\"requestid\":1,\"userid\":2,\"biddate\":null}],\"totalbids\":3}",MyResponse.class);
                //System.out.println("size of bids : "+response.bids.size());

                MyRequestBidAdapter myReqBidAdapter=new MyRequestBidAdapter(RequestDetailsActivity.this,bids,request);

                ListView listView=(ListView)findViewById(R.id.myReqBidList);
                listView.setAdapter(myReqBidAdapter);
//                TwoWayView twoWayView=(TwoWayView) findViewById(R.id.twowayReqBidList);
//                twoWayView.setAdapter(myReqBidAdapter);

                myReqBidAdapter.notifyDataSetChanged();
                dialog.cancel();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }
}
