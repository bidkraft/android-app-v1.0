package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.handson.bidkraft.R;
import com.handson.bidkraft.adapter.MyRequestAdapter;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.model.UserRequest;
import com.handson.bidkraft.responses.GetAvailableResponse;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.responses.GetUserReqResponse;
import com.handson.bidkraft.utility.JsonWebServiceCaller;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyRequestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyRequestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyRequestFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View myFragmentView;
    private MyRequestAdapter myRequestAdapter;
    private List<UserRequest> requests;

    private OnFragmentInteractionListener mListener;

    public MyRequestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyRequestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyRequestFragment newInstance(String param1, String param2) {
        MyRequestFragment fragment = new MyRequestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView= inflater.inflate(R.layout.fragment_my_request, container, false);
        BidkraftApplication application=(BidkraftApplication)getActivity().getApplication();
      //  System.out.println("Access Application User object:"+application.getUser().getUserid());
        new RequestTask().execute(""+application.getUser().getUserid());

        final ImageButton reqListOptIcon=(ImageButton)myFragmentView.findViewById(R.id.reqListSortIcon);
        reqListOptIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popup=inflater.inflate(R.layout.sortfilteroptions,null);
                final PopupWindow popupWindow = new PopupWindow(popup, ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);
                popupWindow.update();
                popupWindow.showAtLocation(reqListOptIcon, Gravity.CENTER,1,1);

                ImageButton ok=(ImageButton)popup.findViewById(R.id.optionsBarOk);
                ImageButton cancel=(ImageButton)popup.findViewById(R.id.optionsBarCancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        RadioGroup radioGroup=(RadioGroup)popup.findViewById(R.id.optionsBarRG);
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        RadioButton selectButton=(RadioButton) popup.findViewById(selectedId);
                        selectButton.setChecked(true);
                        if(selectButton.getText().toString().equalsIgnoreCase("Bids : Low to High"))
                        {
                            Collections.sort(requests,UserRequest.BidComparatorLow2High);
                            myRequestAdapter.notifyDataSetChanged();
                        }
                        else if(selectButton.getText().toString().equalsIgnoreCase("Bids: High to Low"))
                        {
                            Collections.sort(requests,UserRequest.BidComparatorHigh2Low);
                            myRequestAdapter.notifyDataSetChanged();
                        }
                    }
                });

            }
        });

        return  myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class RequestTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(getActivity());
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.GETUSERREQUESTS);
            Map<String,String> keys=new HashMap<>();
            keys.put("userid",params[0]);
            request.getEntities().put(ServiceKeys.GETUSERREQUESTS, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.GETUSERREQS, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println(" MY requests, Response from Url: " + result);
                GetUserReqResponse reqResponse = mapper.readValue(result, GetUserReqResponse.class);
                dialog.cancel();
                requests=reqResponse.getEntities().get("GETUSERREQ").get("activeRequests");
                myRequestAdapter=new MyRequestAdapter(getView().getContext(),requests);
                ListView listView=(ListView) getView().findViewById(R.id.myReqList);
                listView.setAdapter(myRequestAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent=new Intent(getActivity().getApplicationContext(),RequestDetailsActivity.class);
                        intent.putExtra("requestDetails",requests.get(position));
                        startActivity(intent);
                        //Toast.makeText(getActivity().getApplicationContext(),"Request Detail Activity:"+requests.get(position).getRequestid(),Toast.LENGTH_LONG).show();
                    }
                });

                dialog.cancel();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }
}
