package com.handson.bidkraft.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.handson.bidkraft.Manifest;
import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.constants.ServiceKeys;
import com.handson.bidkraft.constants.WebServicesList;
import com.handson.bidkraft.model.KraftRequest;
import com.handson.bidkraft.model.LoginResponse;
import com.handson.bidkraft.utility.JsonWebServiceCaller;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button login;
    Button registration;
    EditText uname;
    EditText pwd;

    int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.MAPS_RECEIVE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.MAPS_RECEIVE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.MAPS_RECEIVE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        login=(Button)findViewById(R.id.loginBtn);
        uname =(EditText)findViewById(R.id.username);
        pwd=(EditText)findViewById(R.id.password);
        registration=(Button)findViewById(R.id.regBtn);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoginTask().execute(uname.getText().toString(),pwd.getText().toString());
            }
        });

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),RegistrationActivity.class);
                startActivity(intent);
            }
        });


//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        NotificationCompat.Builder builder=new NotificationCompat.Builder(this);
//        builder.setContentTitle("Sample Notification");
//        builder.setContentText("Sample Content");
//        builder.setSmallIcon(R.drawable.bidicon);
//        builder.setNumber(1);
//
//        notificationManager.notify(1,builder.build());
    }

    class LoginTask extends AsyncTask<String, Integer, String>
    {
        private ProgressDialog dialog=new ProgressDialog(LoginActivity.this);
        ObjectMapper mapper = new ObjectMapper();
        String urlParameters="";

        @Override
        protected void onPreExecute() {
            dialog.setMessage("loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            KraftRequest request=new KraftRequest();
            request.setMasterKey(ServiceKeys.LOGIN);
            Map<String,String> keys=new HashMap<>();
            keys.put("email",params[0]);
            keys.put("password",params[1]);
            request.getEntities().put(ServiceKeys.LOGIN, keys);
            try {
                urlParameters = mapper.writeValueAsString(request);
                System.out.println("Req :"+urlParameters);
            }catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return JsonWebServiceCaller.call(WebServicesList.LOGIN, urlParameters);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                System.out.println("Response from Url: " + result);
                LoginResponse loginResponse = mapper.readValue(result, LoginResponse.class);
                dialog.cancel();
                //Toast.makeText(getApplicationContext(),loginResponse.getStatus(),Toast.LENGTH_LONG).show();
                if(loginResponse.getStatus().equals("success"))
                {
                    final BidkraftApplication application= (BidkraftApplication) getApplicationContext();
                    application.setUser(loginResponse.getEntities().get(ServiceKeys.LOGIN));
                    application.setAddress("1049W 49th Street");
                    application.setCity("Norfolk");
                    application.setZipcode("23508");
                    Intent intent=new Intent(getApplicationContext(),NavHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        }

    }


}
