package com.handson.bidkraft.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.handson.bidkraft.R;
import com.handson.bidkraft.application.BidkraftApplication;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.responses.UserRequestResponse;

public class RequestInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_info);

        Button placeBidSbmt=(Button)findViewById(R.id.placeBidSbmt);

        setTitle("Request Info");
        UserRequestResponse requestDetails= (UserRequestResponse) getIntent().getSerializableExtra("reqInfo");

        final BidkraftApplication application= (BidkraftApplication) getApplicationContext();
        if(requestDetails.getBids()!=null)
        {
            
        }
        if(application.getUser().getUserid()==requestDetails.getRequest().getUserid())
        {
            placeBidSbmt.setVisibility(View.GONE);
        }
        else
        {
            placeBidSbmt.setVisibility(View.VISIBLE);
        }



        TextView textView=(TextView)findViewById(R.id.reqInfoTitle);
        textView.setText(requestDetails.getRequest().getTitle());

        TextView textDesc=(TextView)findViewById(R.id.reqInfoDesc);
        textDesc.setText(requestDetails.getRequest().getDescription());

        TextView stDate=(TextView)findViewById(R.id.reqInfoStDate);
        stDate.setText(requestDetails.getRequest().getStart_date().toString());

        TextView endDate=(TextView)findViewById(R.id.reqInfoEndDate);
        endDate.setText(requestDetails.getRequest().getEnd_date().toString());

        TextView address=(TextView)findViewById(R.id.reqInfoAddress);
        address.setText(requestDetails.getRequest().getAddress());

        placeBidSbmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
