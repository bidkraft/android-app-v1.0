package com.handson.bidkraft.utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import com.handson.bidkraft.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by kkallepalli on 4/21/2016.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    public int year;
    public int month;
    public int day;

    private int choosenDate=0;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Bundle bundle=this.getArguments();
        if(bundle!=null)
        {
            choosenDate=bundle.getInt("DATE");
        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        System.out.println("Year in onDateSet method:"+view.getId());
        if(choosenDate==1) {
            TextView startDate = (TextView) getActivity().findViewById(R.id.reqStDt);
            startDate.setText(day + "-" + (month+1) + "-" + year);
        }
        else
        {
            TextView endDate = (TextView) getActivity().findViewById(R.id.reqEndDt);
            endDate.setText(day + "-" + (month+1) + "-" + year);
        }
        this.year=year;
        this.month=month;
        this.day=day;
    }
}
