package com.handson.bidkraft.utility;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by kkallepalli on 4/21/2016.
 */
public class KraftMarker implements ClusterItem {

    private final LatLng mPosition;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    private String title;
    private String snippet;

    public KraftMarker(double lat, double lng,String title,String snippet) {
        mPosition = new LatLng(lat, lng);
        this.title=title;
        this.snippet=snippet;
    }


    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
