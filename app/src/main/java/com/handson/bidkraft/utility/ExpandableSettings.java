package com.handson.bidkraft.utility;

/**
 * Created by kkallepalli on 4/8/2016.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableSettings {
    public static LinkedHashMap getData() {
        LinkedHashMap expandableListDetail = new LinkedHashMap<>();

        List notification = new ArrayList();
        notification.add("Enable Notification");
        notification.add("Notify New Request");
        notification.add("Notify New Bid");

        List profile = new ArrayList();
        profile.add("Contact Info");
        profile.add("Address Info");

        List payment = new ArrayList();
        payment.add("Add New Card");
        payment.add("Delete Card");

        List bgc = new ArrayList();
        bgc.add("Status");
        bgc.add("Resend Request");

        List about = new ArrayList();
        about.add("Version : 1.0.0");
        about.add("Update Info");

        List refer = new ArrayList();
        refer.add("Invite a friend");

        expandableListDetail.put("Notifications", notification);
        expandableListDetail.put("Profile", profile);
        expandableListDetail.put("Payment", payment);
        expandableListDetail.put("BGC", bgc);
        expandableListDetail.put("About", about);
        expandableListDetail.put("Referral", refer);
        return expandableListDetail;
    }
}