package com.handson.bidkraft.constants;

public class ServiceKeys {
	
	public static final String LOGIN="LOGIN";
	public static final String REGISTRATION="REGISTRATION";
	public static final String VERIFICATION="VERIFICATION";
	public static final String CREQ="CREQ";//REQUEST 
	public static final String CBID="CBID";//CREATE Bid 
	public static final String ABID="ABID";//ACCEPT BID
	public static final String UBID="UBID";//UPDATE BID
	public static final String GETAVAILABLEREQUESTS="GETAVAILABLEREQ";
	public static final String GETUSERREQUESTS="GETUSERREQ";
	public static final String GETUSERBIDS="GETUSERBID";
	public static final String GETREQUESTBIDS="GETREQUESTBID";
	
	public static final String BGC_INTIAL="INITIALIZED";
	

}
