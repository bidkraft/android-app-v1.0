package com.handson.bidkraft.constants;

/**
 * Created by kkallepalli on 4/1/2016.
 */
public class WebServicesList {

    public static final String LOGIN="http://52.27.216.60:8080/BidKraftServices/rest/do/login";
    public static final String REGISTRATION="http://52.27.216.60:8080/BidKraftServices/rest/do/registration";
    public static final String GETAVAILABLEREQS="http://52.27.216.60:8080/BidKraftServices/rest/do/getavailreq";
    public static final String VERIFICATION="http://52.27.216.60:8080/BidKraftServices/rest/do/verification/1/b9aafd2e4f1641b2a4ae7d807c4c4d00";
    public static final String CREATEREQUEST="http://52.27.216.60:8080/BidKraftServices/rest/do/creq";
    public static final String CREATEBID="http://52.27.216.60:8080/BidKraftServices/rest/do/cbid";
    public static final String ACCEPTBID="http://52.27.216.60:8080/BidKraftServices/rest/do/abid";
    public static final String GETAVAILREQ="http://52.27.216.60:8080/BidKraftServices/rest/do/getavailreq";
    public static final String GETUSERREQS="http://52.27.216.60:8080/BidKraftServices/rest/do/getuserreq";
    public static final String GETUSERBIDS="http://52.27.216.60:8080/BidKraftServices/rest/do/getuserbids";
    public static final String GETREQBIDS="http://52.27.216.60:8080/BidKraftServices/rest/do/getreqbids";
}
