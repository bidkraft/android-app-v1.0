package com.handson.bidkraft.entities;

import java.io.Serializable;
import java.util.Date;

public class Bid implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long bidid;
	private long requestid;
	private String description;	
	private double amount;	
	private long userid;
	private String status;
	private int currency;
	private String biddate;
	private User user;

	public Bid() {
	}

	public Bid(long bidid, long requestid, String description, double amount, long userid, String status, int currency, String biddate,User user) {
		this.bidid = bidid;
		this.requestid = requestid;
		this.description = description;
		this.amount = amount;
		this.userid = userid;
		this.status = status;
		this.currency = currency;
		this.biddate = biddate;
		this.user=user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getBidid() {
		return bidid;
	}
	public void setBidid(long bidid) {
		this.bidid = bidid;
	}

	public long getRequestid() {
		return requestid;
	}
	public void setRequestid(long requestId) {
		this.requestid = requestId;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public int getCurrency() {
		return currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	public String getBiddate() {
		return biddate;
	}

	public void setBiddate(String biddate) {
		this.biddate = biddate;
	}
}
