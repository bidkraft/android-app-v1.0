package com.handson.bidkraft.entities;

/**
 * Created by kkallepalli on 6/7/2016.
 */
public class Query {

    private String type;

    public Query(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
