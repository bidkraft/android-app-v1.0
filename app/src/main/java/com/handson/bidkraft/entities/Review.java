package com.handson.bidkraft.entities;

import java.util.Date;

/**
 * Created by kkallepalli on 6/26/2016.
 */
public class Review {
    private String reviewer;
    private String desc;
    private Date reviewedOn;

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getReviewedOn() {
        return reviewedOn;
    }

    public void setReviewedOn(Date reviewedOn) {
        this.reviewedOn = reviewedOn;
    }
}
