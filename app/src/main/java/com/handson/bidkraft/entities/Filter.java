package com.handson.bidkraft.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by kkallepalli on 6/7/2016.
 */
public class Filter<T> {

    private String type;
    private String operator;
    private List<T> queries;

    public  Filter()
    {
        queries=new ArrayList<>();
    }

    public Filter(String type, String operator, List<T> queries) {
        this.type = type;
        this.operator = operator;
        this.queries = queries;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<T> getQueries() {
        return queries;
    }

    public void setQueries(List<T> queries) {
        this.queries = queries;
    }
}
