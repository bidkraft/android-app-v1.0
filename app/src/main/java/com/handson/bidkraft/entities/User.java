package com.handson.bidkraft.entities;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;


public class User implements Serializable{
	
	private long userid;
	private String email;
	private String password;
	private double lat;
	private double lon;
	private String bgcstatus;
	private int rating;
	private byte[] image;

	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getBgcstatus() {
		return bgcstatus;
	}
	public void setBgcstatus(String bgcstatus) {
		this.bgcstatus = bgcstatus;
	}

	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
}
