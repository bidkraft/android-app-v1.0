package com.handson.bidkraft.entities;

/**
 * Created by kkallepalli on 6/7/2016.
 */
public class GeoQuery extends Query{

    private String distance;
    private String longitude;
    private String latitude;

    public GeoQuery(String distance, String longitude, String latitude,String type) {
        super(type);
        this.distance = distance;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
