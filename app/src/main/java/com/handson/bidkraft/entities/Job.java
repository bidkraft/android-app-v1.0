package com.handson.bidkraft.entities;

import java.io.Serializable;

public class Job implements Serializable {

	private long job_id;
	private long request_id;
	private long bid_id;
	private String status;

	public long getJob_id() {
		return job_id;
	}
	public void setJob_id(long job_id) {
		this.job_id = job_id;
	}

	public void setRequest_id(long request_id) {
		this.request_id = request_id;
	}
	public long getRequest_id() {
		return request_id;
	}

	public void setBid_id(long bid_id) {
		this.bid_id = bid_id;
	}
	public long getBid_id() {
		return bid_id;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
}
