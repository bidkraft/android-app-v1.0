package com.handson.bidkraft.model;

import com.handson.bidkraft.entities.Bid;

import java.util.List;
import java.util.Map;

/**
 * Created by kkallepalli on 4/12/2016.
 */
public class MyBidResponse {

    private String status;
    private List<String> keys;
    private Map<String,Map<String,List<UserRequest>>> entities;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public Map<String, Map<String,List<UserRequest>>> getEntities() {
        return entities;
    }

    public void setEntities(Map<String, Map<String,List<UserRequest>>> entities) {
        this.entities = entities;
    }
}
