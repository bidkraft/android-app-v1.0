package com.handson.bidkraft.model;

import org.codehaus.jackson.annotate.JsonIgnore;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by kkallepalli on 5/24/2016.
 */
public class LuisResponse {

    String query;
    List<Intent> intents;
    List<Entity> entities;

    public LuisResponse()
    {
        this.intents=new ArrayList<Intent>();
        this.entities=new ArrayList<Entity>();
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Intent> getIntents() {
        return intents;
    }

    public void setIntents(List<Intent> intents) {
        this.intents = intents;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }
}
