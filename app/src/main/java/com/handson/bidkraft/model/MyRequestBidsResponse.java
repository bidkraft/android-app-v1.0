package com.handson.bidkraft.model;

import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.responses.BidsResponse;
import com.handson.bidkraft.responses.UserBid;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by kkallepalli on 6/20/2016.
 */
public class MyRequestBidsResponse implements Serializable {

    private String status;
    private List<String> keys;
    private Map<String,BidsResponse> entities;

    public MyRequestBidsResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public Map<String, BidsResponse> getEntities() {
        return entities;
    }

    public void setEntities(Map<String, BidsResponse> entities) {
        this.entities = entities;
    }

}
