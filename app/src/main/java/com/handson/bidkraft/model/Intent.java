package com.handson.bidkraft.model;

/**
 * Created by kkallepalli on 5/24/2016.
 */
public class Intent
{
    public String intent;
    public Double score;

    public Intent()
    {

    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
