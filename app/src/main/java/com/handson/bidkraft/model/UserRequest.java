package com.handson.bidkraft.model;

import com.handson.bidkraft.entities.Bid;
import com.handson.bidkraft.entities.Job;
import com.handson.bidkraft.entities.Request;
import com.handson.bidkraft.responses.UserBid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by kkallepalli on 7/4/2016.
 */
public class UserRequest implements Serializable,Comparable<UserRequest> {

    private Request request;
    private List<UserBid> bids;
    private Job job;

    public UserRequest()
    {
        this.bids=new ArrayList<>();
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public List<UserBid> getBids() {
        return bids;
    }

    public void setBids(List<UserBid> bids) {
        this.bids = bids;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public int compareTo(UserRequest another) {
        return 0;
    }

    public static Comparator<UserRequest> BidComparatorLow2High =new Comparator<UserRequest>() {
        @Override
        public int compare(UserRequest lhs, UserRequest rhs) {

            return lhs.getBids().size()-rhs.getBids().size();
        }
    };

    public static Comparator<UserRequest> BidComparatorHigh2Low =new Comparator<UserRequest>() {
        @Override
        public int compare(UserRequest lhs, UserRequest rhs) {

            return rhs.getBids().size()-lhs.getBids().size();
        }
    };
}
